const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require('webpack')


module.exports = {
  entry: {
    app: './src/App.jsx',

  },
  plugins: [

    new HtmlWebpackPlugin({
      title: 'Output Management',
      hash: true,
      template: 'public/index.html',
    }),
    new webpack.NamedModulesPlugin(),
  ],
  externals: {
    client_config: 'client_config',
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {// style loader
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader',
        ],
      },
      // {
      //   test: /\.(png|svg|jpg|gif)$/,
      //   use: [
      //     'file-loader',
      //   ],
      // },
    ],
  },
}

