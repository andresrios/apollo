const path = require('path')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack')


module.exports = {
  entry: {
      aux: './externals/index.js',
    },
  output: {
    filename: 'aux.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },
  module: {
     rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
       {// style loader
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader'
         ]
       },
       {
         test: /\.(png|svg|jpg|gif)$/,
         use: [
           'file-loader'
         ]
       }
     ]
   }
};