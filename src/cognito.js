import {
  CognitoUserPool,
  CognitoUser,
  AuthenticationDetails,
} from 'amazon-cognito-identity-js'
// andresrios@yu-track.com
import bluebird from 'bluebird'
import aws from 'aws-sdk'
import jwt from 'jsonwebtoken'

aws.config.update({
  accessKeyId: 'xxx',
  secretAccessKey: 'xxx',
  region: 'us-east-1',
})

const { CognitoIdentityServiceProvider } = aws

const pool = {
  UserPoolId: 'xxx',
  ClientId: 'xxx',
}

const userPool = new CognitoUserPool(pool)

const authenticateUserPromise = (email, password, newPassword, newPassword2, callback) => {
  const authData = {
    Username: email,
    Password: password,
  }
  const authDetails = new AuthenticationDetails(authData)
  const userData = {
    Username: email,
    Pool: userPool,
  }
  const cognitoUser = new CognitoUser(userData)
  cognitoUser.authenticateUser(authDetails, {
    onSuccess: result => {
      const token = result.idToken.payload.email
      const signed = jwt.sign({ email: token }, 'secret')
      sessionStorage.setItem('userToken', signed)
      callback(null, result)
    },
    onFailure: err => {
      console.log(new Error().stack)
      callback(err)
    },
    newPasswordRequired: function (userAttributes) {
      if (!newPassword) {
        callback(null, 'replace')
      } else if (newPassword2 !== newPassword) {
        alert('las passwords no coinciden... reintentando')
      } else {
        cognitoUser.completeNewPasswordChallenge(newPassword, { email: userAttributes.email }, this)
      }
    },
  })
}

export const authenticateUser = bluebird.promisify(authenticateUserPromise)

export const registerPromise = (params, callback) => {
  const {
    parsedFormData,
  } = params
  const {
    email,
  } = parsedFormData

  const Params = {
    UserPoolId: pool.UserPoolId,
    Username: email,

    DesiredDeliveryMediums: [
      'EMAIL',
    ],
    ForceAliasCreation: true,
    TemporaryPassword: 'xxx',
    UserAttributes: [
      {
        Name: 'email',
        Value: email,
      },
      {
        Name: 'email_verified',
        Value: 'true',
      },
    ],
  }
  const client = new CognitoIdentityServiceProvider()
  client.adminCreateUser(Params, (err, data) => {
    if (err) {
      console.log(err)
      callback(true)
    } else {
      callback(null, params)
    }
  })
}

// export const register = bluebird.promisify(registerPromise)

export const register = Params => new Promise((resolve, reject) =>
  registerPromise(Params, err => {
    if (err) {
      resolve(false)
    }
    resolve(true)
  }))

export const createGroup = vals => {
}
export const changePasswordPromise = (email, callback) => {
  const client = new CognitoIdentityServiceProvider()
  const Params = {
    UserPoolId: pool.UserPoolId,
    Username: email,
  }

  client.adminDeleteUser(Params, (err, data) => {
    if (err) console.log(err, err.stack)
    else console.log(data)
    const parsed = { parsedFormData: { email } }
    register(parsed)
    alert('debes revisar tu email para recibir la nueva clave')
  })
}

export const changePassword = bluebird.promisify(changePasswordPromise)
