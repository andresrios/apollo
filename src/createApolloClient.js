import ApolloClient from 'apollo-boost'

import { resolvers } from './resolvers'
import { clientConfig } from './config.loader'

const token = sessionStorage.getItem('userToken')

const { api } = clientConfig

const apiAdr = (process.env.NODE_ENV === 'production')
  ? `https://${api}/graphql`
  : 'http://localhost:4000/graphql'

export const clientCreator = () => {
  const client = new ApolloClient({
    uri: apiAdr,
    fetchOptions: {
      credentials: 'include',
    },
    request: async operation => {
      operation.setContext({
        headers: {
          authorization: token,
        },
      })
    },
    clientState: {
      defaults: {
        formData: JSON.stringify({default: 'a'}),
        selectedName: 'No',
      },
      resolvers,
    },
  })

  return client
}

export const client = clientCreator()
