import React, { Component } from 'react'

import components from './components'

import { authenticateUser } from './cognito'

const ChangePassword = components.ChangePasswordForm


export default class LoginComp extends Component {
  state = {
    error: '',
  }

  processLogin = async vals => {
    try {
      console.log('called')
      const res = await authenticateUser(vals.email, vals.password)
      location.reload()
      console.log(res)
    } catch (e) {
      console.log(e)
      this.setState({
        error: 'Usuario o clave incorrecta',
      })
    }
  }

  render() {
    const token = localStorage.getItem('CognitoIdentityServiceProvider.5g3gnnat4pf6jv0r32kk74ujkc.LastAuthUser')
    if (token) {
      return <div>{this.props.children}</div>
    }
    return <div>
      <span>{this.state.error}</span>
      <ChangePassword
        handleSubmit={this.processLogin}
        omitMutate
        retry
      />
      </div>
  }
}
