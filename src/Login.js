import React, { Component } from 'react'

import components from './components'

import { authenticateUser, changePassword } from './cognito'

const Login = components.loginForm
const ChangePassword = components.changePasswordForm
const Button = components.Button

const resetPasswordFn = async () => {
  const email = document.getElementById('email').value
  console.log('pressed change password', email)
  await changePassword(email)

}
export default class LoginComp extends Component {
  state = {
    error: '',
    email: '',
    replace: false,
  }

  processLogin = async vals => {
    const parsed = JSON.parse(vals.formData.formData)
    const values = parsed.login || {}
    const emailValue = document.getElementById('email')
      ? document.getElementById('email').value
      : this.state.email
    const password = document.getElementById('password')
      ? document.getElementById('password').value
      : this.state.password
    try {
      const email = emailValue
      const {
        newPassword,
        newPassword2,
      } = values
      const res = await authenticateUser(email, password, newPassword, newPassword2)
      if (res === 'replace') {
        this.setState({
          replace: true,
          email,
          password,
        })
      } else {
        location.reload()
      }

      return true
    } catch (e) {
      this.setState({
        error: 'Usuario o clave incorrecta',
      })
    }
  }

  render() {
    
    const token = sessionStorage.getItem('userToken')
    if (token) {
      document.body.style.backgroundImage = "url('')"
      return <div>{this.props.children}</div>
    }
    const {
      replace,
    } = this.state
    document.body.style.backgroundImage = "url('login.jpg')"
    const error = this.state.error
      ? <div> Ha ocurrido un error... Deseas resetear tu password?
        <Button onClick={resetPasswordFn}>Reset</Button>
        </div>
      : <span></span>
    const ret = (replace)
      ? <div>
      <span>Debe crear una nueva password</span>
      <ChangePassword
        handleSubmit={this.processLogin}
        omitMutate
        retry
        formName='login'

      />
      </div>
      : <div>
      <Login
        handleSubmit={this.processLogin}
        omitMutate
        retry
        formName='login'
      />
      <div>{error}</div>
      </div>
    
    return ret
  }
}
