

import React from 'react'
import 'bootstrap/dist/css/bootstrap.css'
import { render } from 'react-dom'
import { ApolloProvider, Query } from 'react-apollo'
import gql from 'graphql-tag'
import { BrowserRouter as Router } from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker'
import components from './components'
import { Routes } from './Router.jsx'

import { clientConfig } from './config.loader'
import { client } from './createApolloClient'
import Login from './Login'

const { routeArr } = clientConfig

const { AppContainer } = components
const currUserQuery = gql`
  query($token: String!){
  currUser (token: $token){
    privileges
    email
  }
}`

const token =
  sessionStorage.getItem('userToken')
render(
  (
  <ApolloProvider client={client}>
      <Router>
        <Login>
        <Query
          query={currUserQuery}
          variables={{ token }}
          notifyOnNetworkStatusChange
          >
          {({ loading, error, data }) => {
            if (loading) return 'Loading...'
          if (error) return `Error! ${error.message}`
          
          const { privileges } = data.currUser[0]
        return (
            <AppContainer
              routeArr={routeArr}
              Routes={Routes}
              curruser={ data.currUser[0]}
              privileges={privileges}
            />
          )
        }}
        </Query>
      </Login>
      </Router>
  </ApolloProvider>
  ), document.getElementById('app'),
)

registerServiceWorker()

