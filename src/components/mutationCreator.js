import _ from 'lodash'
import gql from 'graphql-tag'
import { clientConfig } from '../config.loader'

const { formArray } = clientConfig
const createMutationStr = (name, fields) => {
  const params = _.map(fields, f => {
    const result = `$${f}: String`
    return result
  })
  const returned = _.map(fields, f => {
    const result = `${f}: $${f}`
    return result
  })
  const queryString = `
    mutation ${name}(
      ${params}
    ){
      ${name}(${returned})
      {${fields}}
    }
  `
  const result = gql`${queryString}`
  return result
}

const mutationObj = _.mapKeys(formArray, dataSource => {
  if (dataSource.omitMutation) {
    return null
  }
  const {
    mainMut,
  } = dataSource
  if (!mainMut) {
    console.error('There is no main mut in ', dataSource)
  }
  return mainMut
})


const mutationArr = _.mapValues(mutationObj, dataSource => {
  const {
    mainMut,
    fields,
  } = dataSource
  if (!mainMut) {
    return null
  }
  const fieldNames = _.map(fields, f => f.name)
  return createMutationStr(mainMut, fieldNames)
})

export default mutationArr

