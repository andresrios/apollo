import {
  AppContainer,
  Button,
  CardRow,
  Form,
  Loader,
  Table,
  Flex,
  Wistia,
} from 'splinermann-react-components'

export const Components = {
  AppContainer,
  Button,
  CardRow,
  Form,
  Loader,
  Table,
  Flex,
  Wistia,
}

