import { graphql, compose } from 'react-apollo'
import _ from 'lodash'

import { clientConfig } from '../config.loader'

import mutations from './mutationCreator'
import queryArr from './queryCreator'
import { Components } from './importedComp'

const { dataSources } = clientConfig

const QUERYS = {
  ...mutations,
  ...queryArr,
}
const { Loader } = Components

const DataComp = () => {
  const mappedQueryArr = _.mapValues(dataSources, (dataSource, key) => {
    const {
      querys,
      mainData,
    } = dataSource
    if (!dataSource.mainData) {
      console.error(`QUERY ${key} DOES NOT HAVE mainData PROP`)
    }
    const aditionalQuerys = _.map(querys, x => {
      const result = { Q: QUERYS[x.Q], P: { name: x.as } }
      return result
    })
    const mappedQuerys = _.concat([
      {
        Q: QUERYS[mainData.name],
        P: {
          name: 'mainData',
          options: ownProps => {
            console.log(ownProps)
            const { params } = ownProps.match
            return { variables: params }
          },
        },
      },
      { Q: QUERYS.localState, P: { name: 'localState' } },
    ], aditionalQuerys)
    const querysToMap = _.compact(_.map(mappedQuerys, query => {
      if (query.Q) {
        const {
          Q,
          P,
        } = query
        return graphql(Q, P)
      }
      console.error('You have an undefined query at ', key)

      return null
    }))
    const connected = compose(querysToMap)(Loader)
    return connected
  })
  return mappedQueryArr
}

export default DataComp()
