import { Components } from './importedComp'

import DataComp from './DataComp.init'
import Mutation from './Mutation.init'

const {
  AppContainer,
  Button,
  Loader,
  Table,
  CardRow,
  Login,
  Wistia,
  Flex,
} = Components

const components = {
  AppContainer,
  Button,
  Loader,
  Table,
  CardRow,
  Login,
  Wistia,
  Flex,
  ...DataComp,
  ...Mutation,
}

export default components
