import _ from 'lodash'
import gql from 'graphql-tag'
import { clientConfig } from '../config.loader'

const {
  dataSources,
  QUERYS,
} = clientConfig


const querysToMap = {
  ...dataSources,
  ...QUERYS,
}

const createQueryStr = obj => {
  const {
    name,
    fields,
    local,
    params,
  } = obj
  if (local) {
    const localString = `{${fields}}`
    return gql`${localString}`
  }
  if (params) {
    const paramsMap = _.map(params, f => {
      const result = `$${f}: String`
      return result
    })
    const returned = _.map(params, f => {
      const result = `${f}: $${f}`
      return result
    })
    const queryString = `
    query ${name}(
      ${paramsMap}
    ){
      ${name}(${returned})
      {${fields}}
    }
  `
    const result = `${queryString}`
    return gql`${result}`
  }
  const queryString = `
    query {
      ${name}{
        ${fields}
      }
    }
  `
  const result = `${queryString}`
  return gql`${result}`
}

const queryObj = _.mapKeys(querysToMap, dataSource => {
  const {
    mainData,
  } = dataSource
  return mainData.name
})

const queryArr = _.mapValues(queryObj, dataSource => {
  const {
    mainData,
  } = dataSource
  const str = createQueryStr(mainData)
  return str
})
export default queryArr

