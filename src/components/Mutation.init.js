import { graphql, compose } from 'react-apollo'
import _ from 'lodash'

import { Components } from './importedComp'

import mutations from './mutationCreator'
import queryArr from './queryCreator'
import { clientConfig } from '../config.loader'


import {
  createFn,
  sendValueToLocalState,
} from '../utils'

import {
  formDataQuery,
  setFormDataMutation,
} from './localQuerys'

import { createHelper } from './createForm.helper'
import { registerHelper } from './registerForm.helper'

const { formArray } = clientConfig

const QUERYS = {
  ...mutations,
  ...queryArr,
}

const { Form } = Components
const empty = () => {}

const prepareFields = (fields, formName) => {
  const mappedFields = _.map(fields, field => {
    const nextFn = field.onChange || empty
    const type = field.type || 'text'
    const result = {
      ...field,
      onChange: (value, setFormData, data) => {
        sendValueToLocalState(value, setFormData, field.name, data, formName)
        nextFn(value, setFormData, data, formName, field.name)
      },
      type,
    }

    return result
  })

  return mappedFields
}

const MutationComp = () => {
  const mutationArray = _.mapValues(formArray, (form, key) => {
    if (!form) {
      console.error('Form ', key, ' is undefined')
    }
    const {
      create,
      file,
      fields,
      mainMut,
      omitMutation,
      registerFn,
      updateQuerys,
    } = form
    if (!mainMut && !omitMutation) {
      console.error('mainMut is not defined on', key)
    }
    const mutationName = mainMut || ''
    const formWithFields = (fields)
      ? {
        ...form,
        fields: prepareFields(fields, mutationName),
      }
      : form
    const formWithSubmmit = (create)
      ? createHelper({
        createFn,
        file,
        formWithFields,
        mutationName,
        updateQuerys,
      })
      : (registerFn)
        ? registerHelper({
          createFn,
          file,
          formWithFields,
          mutationName,
          registerFn,
          updateQuerys,
        })
        : formWithFields
    const ConnectFormWrapper = props => {
      const connectedForm = Form(props, formWithSubmmit)
      return connectedForm
    }
    const dataLoadQuerys = _.compact(_.map(fields, f => {
      if (f.type === 'select' && f.config.dataFrom) {
        return f.config.dataFrom.Q
      }
      return null
    }))
    if (formWithSubmmit) {
      if (omitMutation) {
        const toCompose2 = [
          graphql(setFormDataMutation, { name: 'setFormData' }),
          graphql(formDataQuery, { name: 'formData' }),
        ]
        const connected2 = compose(toCompose2)(ConnectFormWrapper)
        return connected2
      }
      const Q = QUERYS[mutationName]
      if (!Q) {
        console.error(`mutation ${mutationName} is not defined in ${_.keys(QUERYS)}`)
      }
      const toCompose = [
        graphql(Q, { name: mutationName }),
        graphql(setFormDataMutation, { name: 'setFormData' }),
        graphql(formDataQuery, { name: 'formData' }),
      ]
      if (dataLoadQuerys.length) {
        _.map(dataLoadQuerys, Dl => {
          const DlQ = QUERYS[Dl]
          if (!DlQ) {
            console.error(`query ${Dl} is not defined in ${_.keys(QUERYS)}`)
          }
          const Gql = graphql(
            DlQ,
            { name: Dl },
          )
          toCompose.push(Gql)
        })
      }
      const connected = compose(toCompose)(ConnectFormWrapper)
      return connected
    }

    return ConnectFormWrapper
  })
  console.log(mutationArray)
  return mutationArray
}

export default MutationComp()
