import { createFn } from './crud'

import { sendValueToLocalState } from './forms'

export {
  createFn,
  sendValueToLocalState,
}
